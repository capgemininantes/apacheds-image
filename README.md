# Apache-DS OpenLDAP server

Build image from:
```sh
docker build -t cappic/apacheds ./
```

Run image with:
```sh
docker run --name apacheds -d -p 10389:10389 -e DOMAIN_SUFFIX="dc=mydomain,dc=com" -e LDAP_ADMIN_PASSWORD="MyPa$$word" cappic/apacheds
```

#### --env
```sh
DOMAIN_SUFFIX: The domain name for LDAP (default="dc=example,dc=com")
LDAP_ADMIN_PASSWORD: The LDAP admin password (default="secret")
USERS_OU: The users Organizational Unit value (default="ou=users")
GROUPS_OU: The groups Organizational Unit value (default="ou=groups")
USERS_DN: The base DN for users (default="${USERS_OU},${DOMAIN_SUFFIX}" if ${DOMAIN_SUFFIX} is defined, "ou=users,ou=system" otherwise)
GROUPS_DN: The base DN for groups (default="${GROUPS_OU},${DOMAIN_SUFFIX}" if ${DOMAIN_SUFFIX} is defined, "ou=groups,ou=system" otherwise)
```
