# Create custom Users or Group lists

Some files can be placed in the custom volume (container `/root/custom`) in order to execute script at server start. You have to use container volume `/root/custom`.

The logs are store in the file `/root/custom/initialization_<YYYYMMDD>.log` (with `YYYYMMDD` as running day date).

Once the file has been treated it is moved in `/root/custom/done` directory.

## Users
### List
The file to create is `users.lst`.

The format of an user in this file is:
```
<UID>|<MAIL>|<DISPLAY_NAME>|<FIRSTNAME>|<LASTNAME>|<PASSWORD>|<GROUP>
```
Where `GROUP` is optional.
If the group does not exist it will be created with the user as member.

### LDIF
The file to create is `users.ldif`.

A standard LDIF file to create users.
## Groups
### List
The file to create is `groups.lst`.

The format of an user in this file is:
```
<GROUP_NAME>|<MEMBERS>
```
Where `MEMBERS` is optional and the different members must be separated by comma (`,`).

### LDIF
The file to create is `groups.ldif`.

A standard LDIF file to create groups.

