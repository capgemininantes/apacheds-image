FROM openjdk:7-jdk
MAINTAINER bgohier

RUN apt-get update && apt-get install -y ldap-utils net-tools

ENV APACHE_DS_VERSION=2.0.0-M19
ENV APACHE_DS_WORKDIR=/var/lib/apacheds-${APACHE_DS_VERSION}/default
ENV APACHE_DS_CUSTOMDIR=/root/custom
ENV APACHE_DS_CUSTOM_DONE=${APACHE_DS_CUSTOMDIR}/done
ENV APACHE_DS_LOGDIR=/root/logs

RUN mkdir -p ${APACHE_DS_CUSTOM_DONE}/done && chmod -R 777 ${APACHE_DS_CUSTOMDIR}
RUN mkdir -p ${APACHE_DS_LOGDIR}/done && chmod -R 777 ${APACHE_DS_LOGDIR}

ADD http://archive.apache.org/dist/directory/apacheds/dist/${APACHE_DS_VERSION}/apacheds-${APACHE_DS_VERSION}-amd64.deb /tmp/apacheds.deb
RUN chmod +x /tmp/apacheds.deb && dpkg -i /tmp/apacheds.deb 

COPY configs/* ${APACHE_DS_WORKDIR}/conf/
RUN chown apacheds:apacheds ${APACHE_DS_WORKDIR}/conf/*

COPY scripts/* /root/

RUN chmod +x /root/*.sh

VOLUME ${APACHE_DS_CUSTOMDIR}/
VOLUME ${APACHE_DS_LOGDIR}/

EXPOSE 10389 10636

ENTRYPOINT ["/root/start.sh"]
