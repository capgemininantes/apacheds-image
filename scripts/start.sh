#!/bin/bash
#

. /root/default_config.sh

LOG_FILE=${APACHE_DS_LOGDIR}/start_$(date "+%Y%m%d").log

echo "=======================================
Start LDAP Server Log [$(date "+%Y/%m/%d %T")]" >> ${LOG_FILE}

echo "Partition: ${PARTITION_ID}" | tee -a ${LOG_FILE}
echo "Domain: ${DOMAIN_NAME}" | tee -a ${LOG_FILE}
echo "Users: ${USERS_DN}" | tee -a ${LOG_FILE}
echo "Groups: ${GROUPS_DN}" | tee -a ${LOG_FILE}

DEFAULT_USER_FILE=${APACHE_DS_WORKDIR}/conf/default_users.ldif
CONFIG_FILE=${APACHE_DS_WORKDIR}/conf/config.ldif

if [ -f ${CONFIG_FILE}.template ] ; then
	echo "$(eval echo "\"$(cat ${CONFIG_FILE}.template)\"")" > "${CONFIG_FILE}"
	rm -f ${CONFIG_FILE}.template
fi
if [ -f ${DEFAULT_USER_FILE}.template ] ; then
	echo "$(eval echo "\"$(cat ${DEFAULT_USER_FILE}.template)\"")" > "${DEFAULT_USER_FILE}"
	rm -f ${DEFAULT_USER_FILE}.template
fi

TRY=1
INIT_WAIT=30
MAX_TRIES=30
WAITING_TIME=2
STARTED=false

PATTERN="[::]:${LDAP_PORT}"

PID_FILE=${APACHE_DS_WORKDIR}/run/apacheds-default.pid
APACHE_CMD=/etc/init.d/apacheds-${APACHE_DS_VERSION}-default

force_stop()
{
	${APACHE_CMD} stop
	pid=$(ps -eaf | grep "/bin/sh ${APACHE_CMD} console" | cut -d"
" -f 1 | cut -d" " -f8)
	if [ ! -z "${pid}" ] ; then
		kill -15 ${pid}
	fi
	rm -f ${PID_FILE}
}

if netstat -eaplnt | grep -qe ${PATTERN} ; then
	force_stop
elif [ -f ${PID_FILE} ] ; then
	rm -f ${PID_FILE}
	${APACHE_CMD} start
else
	${APACHE_CMD} start
fi

echo "Server is starting..." | tee -a ${LOG_FILE}
sleep ${INIT_WAIT}
while [ "${STARTED}" = "false" -a ${TRY} -le ${MAX_TRIES} ] ; do
	echo "Check (${TRY}) if server is started on port ${LDAP_PORT}..." | tee -a ${LOG_FILE}
	if netstat -eaplnt | grep -qe ${PATTERN} ; then
		STARTED=true
		break
	fi
	echo "Waiting ${WAITING_TIME}s..." | tee -a ${LOG_FILE}
	TRY=$((${TRY} + 1))
	sleep ${WAITING_TIME}
done

RET_CODE=0
if ${STARTED} ; then
	echo "Server is started. Begin initialization script." | tee -a ${LOG_FILE}
	/root/init_ldap.sh
	RET_CODE=$?
else
	echo "Server is not started" | tee -a ${LOG_FILE}
	RET_CODE=1
fi

${APACHE_CMD} stop
${APACHE_CMD} console

echo "End LDAP Server Log [$(date "+%Y/%m/%d %T")]
=======================================" >> ${LOG_FILE}

exit ${RET_CODE}
